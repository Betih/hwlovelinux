## 10 nov 2020

La comanda lsbkl és per extreure informació de les particions, sistemes de fitxers i punts de muntatge:

...
[guest@a08 ~]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part 
[guest@a08 ~]$ 
